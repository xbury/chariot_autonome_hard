from time import sleep


def sauve_logs(bdd, message, source='UNKNOWN'):
    """
    Envoi les ordres execute dans la BDD pour savoir quel message est envoyé
    :param bdd:
    :param message:
    :param source:
    :return:
    """
    bdd.query('INSERT INTO logs (source, message) VALUES ("%s","%s")' % (source, message))
    bdd.commit()


def send_rs232(bdd, serial, msg):
    """
    envoi un message sur le port série(RS232) du arduino
    :param serial:
    :param bdd:
    :param msg:
    :return:
    """
    ack = False
    while not ack:
        serial.write(msg.encode())
        serial.flush()
        print('sending')
        sleep(0.1)
        if serial.inWaiting() > 0:
            received = serial.read(serial.inWaiting()).decode('ascii').strip()
            if received[5:].strip() == msg:
                ack = True
                sauve_logs(bdd, message=msg, source='Send RS232')
        sleep(2)
