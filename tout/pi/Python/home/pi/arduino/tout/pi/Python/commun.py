from time import sleep


def sauve_logs(BDD, message, source='UNKNOWN'):
    """

    :param message:
    :param source:
    :return:
    """
    BDD.query('INSERT INTO logs (source, message) VALUES ("%s","%s")' % (source, message))
    BDD.commit()


def send_rs232(BDD, ser, msg):
    """

    :param ser:
    :param msg:
    :return:
    """
    ack = False
    while not ack:
        ser.write(msg.encode())
        ser.flush()
        print('sending')
        sleep(0.1)
        if ser.inWaiting() > 0:
            received = ser.read(ser.inWaiting()).decode('ascii').strip()
            if received[5:].strip() == msg:
                ack = True
                sauve_logs(BDD, message=msg, source='Send RS232')
        sleep(2)
