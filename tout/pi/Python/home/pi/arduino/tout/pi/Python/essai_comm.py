
#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial
import redis

r = redis.Redis(host='localhost', port=6379, db=0)
ser = serial.Serial('/dev/ttyACM0', 200)
print("CTRL + C pour arrêter")

# Ecriture de chaque message recu
while True:
    print("AV = avancer | AR = reculer | TG = tourner à gauche | TD = tourner à droite | ST = arret")
    msg = input('Message : ')
    ser.write(msg.encode())
    print(ser.readline())
    r.get('alert_distance')
    if ('alert_distance') :
        r.set('alert_distance', 0)
