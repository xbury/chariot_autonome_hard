import redis
import serial
from time import sleep
from MySQLdb import _mysql


from commun import *
from pilotage import *
import myConfig


r = redis.Redis(host='localhost', port=6379, db=0)
ser = serial.Serial('/dev/ttyACM0', 200)

current_client = 1

BDD = _mysql.connect(host="localhost", user="root", passwd="", db="projet")



#     if not 'A6':
#         msg = "AV100"
#         send_rs232(ser, msg)
#     if 'A6':
#         if 'A3' and 'A4':
#             msg = "AV100"
#             send_rs232(ser, msg)
#             while 'fin_R5_R4_R3':
#                 msg = "AV100"
#                 send_rs232(ser, msg)
#             while 'R6_R7_fin':
#                 msg = "TG"
#                 send_rs232(ser, msg)
#             while 'fin_R5_R4_R3':
#                 msg = "TG"
#                 send_rs232(ser, msg)
#             while 'fin':
#                 msg = "AV100"
#                 send_rs232(ser, msg)
#         else:
#             while 'fin_A6':
#                 msg = "TG"
#                 send_rs232(ser, msg)
#                 while ''


is_alerte_sur_distance = False
is_contact = False
vitesse_courante = 100


# determination du chemin
liste_course = lecture_liste(BDD, current_client)22
print(liste_course)
allees_a_parcourir = [x['allee_id'] for x in liste_course]
print(allees_a_parcourir)
deplace_chariot(BDD, r, ser, allees_a_parcourir)
exit()
try:
    while True:
        # gestion de la distance
        is_alerte_sur_distance = verifie_distance_sonar(r, ser, is_alerte_sur_distance)

        # Lecture des QR-codes
        qr_code = r.get('qr_code')
        if qr_code != b'':
            gestion_qr_code(qr_code)

        # lecture des messages en provenance de l'arduino sur a liaison serie
        if ser.inWaiting() > 0:
            data_in = ser.read(ser.inWaiting()).decode('ascii').strip()

            # est-ce que arduino a detecté un contact ?
            if data_in == b'Appui':
                gestion_contact(True)
                is_contact = True
            if data_in == b'Fin appui':
                is_contact = False

        sleep(0.5)

except KeyboardInterrupt:
    exit()
