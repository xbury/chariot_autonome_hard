from commun import *
from myConfig import *


def alerte_distance(BDD, ser, distance, seuil, vitesse_courante, is_contact):
    """
Si le capteur ultrason détecte un obstacle à moins 20 cm et 10 cm

    :param ser: liaison serie
    :param distance: distance mesuree
    :param seuil: seuil de distance atteint
    :return:
    """
    new_vitesse = vitesse_courante * AV_SPEED[seuil]
    if not is_contact:
        msg = "AV " + str(new_vitesse)
        send_rs232(ser, msg)

        print("alerte distance mini %s :  %s cm " % (seuil, distance))
        sauve_logs(BDD, source="orchestrateur", message="alerte distance seuil %s : %s cm" % (seuil, distance))
    else:
        msg = "ST"
        send_rs232(BDD, ser, msg)
    return new_vitesse


def gestion_contact(BDD, appui):
    """
Si l'arduino détecte un contact (bouton poussoir)

    :param appui:
    :return:
    """
    print("Contact")
    if appui:
        sauve_logs(BDD, message="Contact Arduino avant", source="gestion_contact")
        vitesse_courante = 0


def verifie_distance_sonar(BDD, r, ser, alerte_sur_distance):
    """
Le raspberry vérfie que la distance est bonne

    :param r:
    :param ser:
    :param alerte_sur_distance:
    :return:
    """
    if r.get('distance') is not None:
        distance = float(r.get('distance'))
        if 0 < distance < DISTANCE_MINI_1 and alerte_sur_distance != DISTANCE_MINI_1:  # 10
            alerte_distance(BDD, ser, distance, DISTANCE_MINI_1)
            alerte_sur_distance = DISTANCE_MINI_1
        if DISTANCE_MINI_1 < distance <= DISTANCE_MINI_2 and alerte_sur_distance != DISTANCE_MINI_2:  # 20
            alerte_distance(BDD, ser, distance, DISTANCE_MINI_2)
            alerte_sur_distance = DISTANCE_MINI_2
        if distance > DISTANCE_MINI_2:
            alerte_sur_distance = False
    return alerte_sur_distance


def gestion_qr_code(r, qr_code):
    """
Le Pi va chercher le dernier QR-Code lu
    :param qr_code:
    :return:
    """
    print(qr_code)
    r.set('qr_code', '')


# Lecture de la liste des courses
def lecture_liste(BDD, current_client):
    """
Le Pi lit la les ID des allées
    :return:
    """
    c = BDD.cursor()
    c.execute(
        'SELECT lc.produit_id, p.nom, a.allee_id, a.nom '
        'FROM `liste_courses` lc '
        'JOIN produit p ON p.produit_id=lc.produit_id '
        'JOIN allee a on a.allee_id=p.allee_id '
        'WHERE client_id=%s '
        'ORDER BY a.allee_id' % current_client)
    columns = [col[0] for col in c.description]
    return [dict(zip(columns, row)) for row in c.fetchall()]


def circuit_simple(BDD, ser):
    """
C'est le circuit le plus du robot (Allée 1 et/ou 2 et/ou 3)
    :return:
    """
    msg = "AV100"
    send_rs232(BDD, ser, msg)


def fin_par_le_milieu(BDD, r, ser):
    """
Le circuit où les allées à visiter sont 1 et/ou 2 et/ou 3 et 6
    :param r:
    :return:
    """
    msg = "AV100"
    send_rs232(BDD, ser, msg)
    while r.get('qr_code') != 'fin_A6':
        sleep(0.5)
    # on a atteint fin_A6
    msg = "TG50"
    send_rs232(BDD, ser, msg)
    while r.get('qr_code') != 'fin':
        sleep(0.5)
    msg = "TG50"
    send_rs232(BDD, ser, msg)
    while r.get('qr_code') != 'sortie':
        sleep(0.5)
    msg = "AV50"
    send_rs232(BDD, ser, msg)


def circuit_complet(BDD, r, ser):
    """
Le circuit où les allées à visiter sont 1 et/ou 2 et
    :param BDD:
    :param r:
    :param ser:
    :return:
    """
    pass


# determination du chemin
def deplace_chariot(BDD, r, ser, allees):
    """

    :param r:
    :param allees:
    :return:
    """
    if 6 in allees:
        print('passage par allee 6')
        if 3 in allees or 4 in allees:
            print('circuit complet')
            circuit_complet(BDD, r, ser)
        else:
            print('fin par le milieu')
            fin_par_le_milieu(BDD, r, ser)
    else:
        print('circuit simple')
        circuit_simple(BDD, r, ser)
