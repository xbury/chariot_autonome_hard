import redis
import sys
import time
from logs import *

r = redis.Redis(host='localhost', port=6379, db=0)

while True:
    for line in sys.stdin:
        print(line)
        r.set('qr_code', line)
        sauve_logs(line, "qr_code")

    time.sleep(0.5)
