import redis
import serial
from time import sleep
import MySQLdb


from commun import *
from pilotage import *
import myConfig


redis = redis.Redis(host='localhost', port=6379, db=0)
serial = serial.Serial('/dev/ttyACM0', 200)
bdd = MySQLdb.connect(host="localhost", user="root", passwd="", db="projet")

current_client = 1
is_alerte_sur_distance = False
is_contact = False
vitesse_courante = 100

# determination du chemin
liste_course = lecture_liste(bdd, current_client)
print(liste_course)
allees_a_parcourir = [x['allee_id'] for x in liste_course]
print(allees_a_parcourir)
deplace_chariot(bdd=bdd, redis=redis, serial=serial, allees=allees_a_parcourir)

try:
    while True:
        # gestion de la distance
        is_alerte_sur_distance = verifie_distance_sonar(bdd=bdd, redis=redis, serial=serial,
                                                        alerte_sur_distance=is_alerte_sur_distance,
                                                        vitesse_courante=vitesse_courante, is_contact=is_contact)

        # Lecture des QR-codes
        qr_code = redis.get('qr_code')
        if qr_code != b'':
            gestion_qr_code(redis=redis, qr_code=qr_code)

        # lecture des messages en provenance de l'arduino sur a liaison serie
        if serial.inWaiting() > 0:
            data_in = serial.read(serial.inWaiting()).decode('ascii').strip()

            # est-ce que arduino a detecté un contact ?
            if data_in == b'Appui':
                vitesse_courante = gestion_contact(bdd=bdd, appui=True, vitesse_courante=vitesse_courante)
                is_contact = True
            if data_in == b'Fin appui':
                is_contact = False

        sleep(0.5)

except KeyboardInterrupt:
    exit()
