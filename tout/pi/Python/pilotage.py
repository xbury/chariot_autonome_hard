from commun import *
from myConfig import *


def alerte_distance(bdd, serial, distance, seuil, vitesse_courante, is_contact):
    """
    Si le capteur ultrason détecte un obstacle à moins 20 cm et 10 cm
    :param is_contact:
    :param vitesse_courante:
    :param bdd:
    :param serial: liaison serie
    :param distance: distance mesuree
    :param seuil: seuil de distance atteint
    :return:
    """
    new_vitesse = vitesse_courante * AV_SPEED[seuil]
    if not is_contact:
        msg = "AV " + str(new_vitesse)
        send_rs232(bdd=bdd, serial=serial, msg=msg)

        print("alerte distance mini %s :  %s cm " % (seuil, distance))
        sauve_logs(bdd, source="orchestrateur", message="alerte distance seuil %s : %s cm" % (seuil, distance))
    else:
        msg = "ST"
        send_rs232(bdd=bdd, serial=serial, msg=msg)
    return new_vitesse


def gestion_contact(bdd, appui, vitesse_courante):
    """
    Si l'arduino détecte un contact (bouton poussoir)
    :param vitesse_courante:
    :param bdd:
    :param appui:
    :return:
    """
    print("Contact")
    if appui:
        sauve_logs(bdd, message="Contact Arduino avant", source="gestion_contact")
        vitesse_courante = 0
    return vitesse_courante


def verifie_distance_sonar(bdd, redis, serial, alerte_sur_distance, vitesse_courante, is_contact):
    """
    Le raspberry vérifie que la distance est bonne
    :param is_contact:
    :param vitesse_courante:
    :param bdd:
    :param redis:
    :param serial:
    :param alerte_sur_distance:
    :return:
    """
    if redis.get('distance') is not None:
        distance = float(redis.get('distance'))
        if 0 < distance < DISTANCE_MINI_1 and alerte_sur_distance != DISTANCE_MINI_1:  # 10
            alerte_distance(bdd=bdd, serial=serial, distance=distance, seuil=DISTANCE_MINI_1,
                            vitesse_courante=vitesse_courante, is_contact=is_contact)
            alerte_sur_distance = DISTANCE_MINI_1
        if DISTANCE_MINI_1 < distance <= DISTANCE_MINI_2 and alerte_sur_distance != DISTANCE_MINI_2:  # 20
            alerte_distance(bdd=bdd, serial=serial, distance=distance, seuil=DISTANCE_MINI_2,
                            vitesse_courante=vitesse_courante, is_contact=is_contact)
            alerte_sur_distance = DISTANCE_MINI_2
        if distance > DISTANCE_MINI_2:
            alerte_sur_distance = False
    return alerte_sur_distance


def gestion_qr_code(redis, qr_code):
    """
    Le Pi va chercher le dernier QR-Code lu
    :param redis:
    :param qr_code:
    :return:
    """
    print(qr_code)
    redis.set('qr_code', '')


def lecture_liste(bdd, current_client):
    """
    Lecture de la liste des courses
    Le Pi lit la les ID des allées
    :return:
    """
    c = bdd.cursor()
    c.execute(
        'SELECT lc.produit_id, p.nom, a.allee_id, a.nom '
        'FROM `liste_courses` lc '
        'JOIN produit p ON p.produit_id=lc.produit_id '
        'JOIN allee a on a.allee_id=p.allee_id '
        'WHERE client_id=%s '
        'ORDER BY a.allee_id' % current_client)
    columns = [col[0] for col in c.description]
    return [dict(zip(columns, row)) for row in c.fetchall()]


def circuit_simple(bdd, serial):
    """
    C'est le circuit le plus du robot (Allée 1 et/ou 2 et/ou 3)
    :return:
    """
    msg = "AV100"
    send_rs232(bdd=bdd, serial=serial, msg=msg)


def fin_par_le_milieu(bdd, redis, serial):
    """
    Le circuit où les allées à visiter sont 1 et/ou 2 et/ou 3 et 6
    :param serial:
    :param bdd:
    :param redis:
    :return:
    """
    msg = "AV100"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'fin_A6':
        sleep(0.5)
    # on a atteint fin_A6
    msg = "TG50"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'fin':
        sleep(0.5)
    msg = "TG50"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'sortie':
        sleep(0.5)
    msg = "AV50"
    send_rs232(bdd, serial, msg)


def circuit_complet(bdd, redis, serial):
    """
    Le circuit où les allées à visiter sont 3 et/ou 4 et 6
    :param serial:
    :param redis:
    :param bdd:
    :return:
    """
    msg = "AV100"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'fin_A3_A4':
        sleep(0.5)
    # on a atteint fin_A6
    msg = "AV100"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'A6_fin':
        sleep(0.5)
    # on a atteint fin_A6
    msg = "TG50"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'fin_A3_A4':
        sleep(0.5)
    # on a atteint fin_A6
    msg = "TG50"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'fin':
        sleep(0.5)
    msg = "AV100"
    send_rs232(bdd, serial, msg)
    while redis.get('qr_code') != 'sortie':
        sleep(0.5)
    msg = "AV50"
    send_rs232(bdd, serial, msg)


# determination du chemin
def deplace_chariot(bdd, redis, serial, allees):
    """
    Le raspberry choisit le bon parcours en fonction de la liste de course
    :param bdd:
    :param serial:
    :param redis:
    :param allees:
    :return:
    """
    if 6 in allees:
        print('passage par allee 6')
        if 3 in allees or 4 in allees:
            print('circuit complet')
            circuit_complet(bdd=bdd, redis=redis, serial=serial)
        else:
            print('fin par le milieu')
            fin_par_le_milieu(bdd=bdd, redis=redis, serial=serial)
    else:
        print('circuit simple')
        circuit_simple(bdd=bdd, serial=serial)
