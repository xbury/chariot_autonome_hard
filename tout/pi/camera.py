import redis
import sys
import time

r = redis.Redis(host='localhost', port=6379, db=0)

while True:
    for line in sys.stdin:
        print(line)
        r.set('qr_code', line)
    time.sleep(0.5)
