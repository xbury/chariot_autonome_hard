// Ce programme sert a commander un robot quadrimoteur à partir d'un raspberry pi avec un bouton STOP
// Un retour des actions effectués sont affichés sur l'écran du pi


const int PORT_LED_INTEGREE = 13;

const int PORT_VITESSE_AVANT_DROIT = 5;
const int PORT_SENS_AVANT_DROIT = 8;

const int PORT_VITESSE_AVANT_GAUCHE = 3;
const int PORT_SENS_AVANT_GAUCHE = 4;

const int PORT_VITESSE_ARRIERE_DROIT = 6;
const int PORT_SENS_ARRIERE_DROIT = 7;

const int PORT_VITESSE_ARRIERE_GAUCHE = 11;
const int PORT_SENS_ARRIERE_GAUCHE = 12;

const int VITESSE_ARRET = 0;

const int SENS_AVANT = HIGH;
const int SENS_ARRIERE = LOW;
const int SENS_ARRET = LOW;

const int AV_VITESSE = 100;
const int AR_VITESSE = 100;
const int TOUR_VITESSE = 50;

const int BAUD_VITESSE = 200;
const int TEMPS_ATTENTE = 500;

const int PORT_BOUTON = 2;

int status_bouton = 0; // 1 si passage relache-appui, 0 sinon


String message;

void moteur(char port_VITESSE, int vitesse, char port_sens, char sens) {
   digitalWrite(port_sens, sens);
   analogWrite(port_VITESSE, vitesse);
}

void forward(int vitesse){
   // tous les moteurs en avant
   moteur(PORT_VITESSE_AVANT_DROIT, vitesse, PORT_SENS_AVANT_DROIT,
SENS_AVANT);
   moteur(PORT_VITESSE_AVANT_GAUCHE, vitesse, PORT_SENS_AVANT_GAUCHE,
SENS_AVANT);
   moteur(PORT_VITESSE_ARRIERE_DROIT, vitesse, PORT_SENS_ARRIERE_DROIT,
SENS_AVANT);
   moteur(PORT_VITESSE_ARRIERE_GAUCHE, vitesse,
PORT_SENS_ARRIERE_GAUCHE, SENS_AVANT);
}



void backward(int vitesse){
   // tous les moteurs en arriere
   moteur(PORT_VITESSE_AVANT_DROIT, vitesse, PORT_SENS_AVANT_DROIT,
SENS_ARRIERE);
   moteur(PORT_VITESSE_AVANT_GAUCHE, vitesse, PORT_SENS_AVANT_GAUCHE,
SENS_ARRIERE);
   moteur(PORT_VITESSE_ARRIERE_DROIT, vitesse, PORT_SENS_ARRIERE_DROIT,
SENS_ARRIERE);
   moteur(PORT_VITESSE_ARRIERE_GAUCHE, vitesse,
PORT_SENS_ARRIERE_GAUCHE, SENS_ARRIERE);
}

void turnleft(int vitesse){
   // les moteurs à gauche avancent, ceux de droite sont a l arret
   moteur(PORT_VITESSE_AVANT_DROIT, VITESSE_ARRET,
PORT_SENS_AVANT_DROIT, SENS_ARRET);
   moteur(PORT_VITESSE_AVANT_GAUCHE, vitesse, PORT_SENS_AVANT_GAUCHE,
SENS_AVANT);
   moteur(PORT_VITESSE_ARRIERE_DROIT, VITESSE_ARRET,
PORT_SENS_ARRIERE_DROIT, SENS_ARRET);
   moteur(PORT_VITESSE_ARRIERE_GAUCHE, vitesse,
PORT_SENS_ARRIERE_GAUCHE, SENS_AVANT);
}

void turnright(int vitesse){
   // les moteurs à droite avancent, ceux de gauche sont a l arret
   moteur(PORT_VITESSE_AVANT_DROIT, vitesse, PORT_SENS_AVANT_DROIT,
SENS_AVANT);
   moteur(PORT_VITESSE_AVANT_GAUCHE, VITESSE_ARRET,
PORT_SENS_AVANT_GAUCHE, SENS_ARRET);
   moteur(PORT_VITESSE_ARRIERE_DROIT, vitesse, PORT_SENS_ARRIERE_DROIT,
SENS_AVANT);
   moteur(PORT_VITESSE_ARRIERE_GAUCHE, VITESSE_ARRET,
PORT_SENS_ARRIERE_GAUCHE, SENS_ARRET);
}

void arret(){
   // tous les moteus a l arret
   moteur(PORT_VITESSE_AVANT_DROIT, VITESSE_ARRET,
PORT_SENS_AVANT_DROIT, SENS_ARRET);
   moteur(PORT_VITESSE_AVANT_GAUCHE, VITESSE_ARRET,
PORT_SENS_AVANT_GAUCHE, SENS_ARRET);
   moteur(PORT_VITESSE_ARRIERE_DROIT, VITESSE_ARRET,
PORT_SENS_ARRIERE_DROIT, SENS_ARRET);
   moteur(PORT_VITESSE_ARRIERE_GAUCHE, VITESSE_ARRET,
PORT_SENS_ARRIERE_GAUCHE, SENS_ARRET);
}


void ack(String message){
   Serial.print("ACK : ");
   Serial.println(message);
}


void analyse_message(String message) {
  String ordre;
  int vitesse;
   message.toUpperCase();

   ack(message);

   ordre = message.substring(0,2);
   vitesse = message.substring(2).toInt();

   if (ordre == "AV"){
     if (vitesse == 0) {
       vitesse = AV_VITESSE;
     }
     forward(vitesse);
   }
   else if (ordre == "AR"){
     if (vitesse == 0) {
       vitesse = AV_VITESSE;
     }
     backward(vitesse);
   }
   else if (ordre == "TG"){
     if (vitesse == 0) {
       vitesse = AV_VITESSE;
     }
     turnleft(vitesse);
   }
   else if (ordre == "TD"){
     if (vitesse == 0) {
       vitesse = AV_VITESSE;
     }
     turnright(vitesse);
   }
   else if (ordre == "ST"){
     arret();
   }
   else {
     Serial.println("ERROR");
   }
}


void setup(){
   pinMode(PORT_VITESSE_AVANT_DROIT, OUTPUT);
   pinMode(PORT_VITESSE_AVANT_GAUCHE, OUTPUT);
   pinMode(PORT_VITESSE_ARRIERE_DROIT, OUTPUT);
   pinMode(PORT_VITESSE_ARRIERE_GAUCHE, OUTPUT);
   pinMode(PORT_SENS_AVANT_DROIT, OUTPUT);
   pinMode(PORT_SENS_AVANT_GAUCHE, OUTPUT);
   pinMode(PORT_SENS_ARRIERE_DROIT, OUTPUT);
   pinMode(PORT_SENS_ARRIERE_GAUCHE, OUTPUT);


   Serial.begin(BAUD_VITESSE);

   pinMode(PORT_LED_INTEGREE, OUTPUT);
   digitalWrite(PORT_LED_INTEGREE, LOW);

   pinMode(PORT_BOUTON, INPUT);
   status_bouton = 0;
}


void loop() {
   if (digitalRead(PORT_BOUTON) == LOW) {
     digitalWrite(PORT_LED_INTEGREE, HIGH);
     if (status_bouton == 0) {
      status_bouton = 1;
      Serial.println("Appui");
     }
     arret();
   }
   else {
     digitalWrite(PORT_LED_INTEGREE, LOW);
     if (status_bouton == 1) {
      status_bouton = 0;
      Serial.println("Fin appui");
     }
   }

   if (Serial.available()) {
    message = Serial.readString();
    analyse_message(message);
  }

  delay(TEMPS_ATTENTE);
}
